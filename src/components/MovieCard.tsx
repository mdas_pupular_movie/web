import React, { useState } from "react";
import { Badge, Button, Card, createStyles, Group, Image, Progress, Text } from "@mantine/core";
import { Movie } from "../definitions/movie";
import { useAppDispatch } from "../app/hooks";
import { useInterval } from "@mantine/hooks";
import { addMovieToCart } from "../features/cart/cart_slice";

const useStyles = createStyles((theme) => ({
    button: {
        position: "relative",
        transition: "background-color 150ms ease"
    },

    progress: {
        position: "absolute",
        bottom: -1,
        right: -1,
        left: -1,
        top: -1,
        height: "auto",
        backgroundColor: "transparent",
        zIndex: 0
    },

    label: {
        position: "relative",
        zIndex: 1
    },
    card: {
        backgroundColor: theme.colorScheme === "dark" ? theme.colors.dark[7] : theme.white
    },

    imageSection: {
        padding: theme.spacing.md,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderBottom: `1px solid ${theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3]}`
    },

    section: {
        padding: theme.spacing.md,
        borderTop: `1px solid ${theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3]}`
    },

    icon: {
        marginRight: 5,
        color: theme.colorScheme === "dark" ? theme.colors.dark[2] : theme.colors.gray[5]
    },
    rating: {
        position: "absolute",
        top: theme.spacing.xs,
        right: theme.spacing.xs + 2,
        pointerEvents: "none"
    }
}));

interface MovieCardProps {
    movie: Movie;
    cartId?: string;
}

export function MovieCard({ movie, cartId }: MovieCardProps) {
    const { classes, theme } = useStyles();
    const dispatch = useAppDispatch();
    const [progress, setProgress] = useState(0);
    const [loaded, setLoaded] = useState(false);

    const interval = useInterval(
        () =>
            setProgress((current) => {
                if (current < 100) {
                    return current + 1;
                }

                interval.stop();
                setLoaded(true);

                return 0;
            }),
        20
    );
    return (
        <Card withBorder radius="md" className={classes.card}>
            <Card.Section className={classes.imageSection}>
                <Image src={movie.cover} />
            </Card.Section>
            <Badge className={classes.rating} variant="gradient" gradient={{ from: "indigo", to: "cyan", deg: 60 }}>
                {new Date(movie.year).toDateString()}
            </Badge>
            <Card.Section className={classes.section}>
                <Group spacing={30}>
                    <div>
                        <Text size="xl" weight={700} sx={{ lineHeight: 1 }}>
                            {movie.price} €
                        </Text>
                    </div>
                    <Button
                        radius="xl"
                        style={{ flex: 1 }}
                        className={classes.button}
                        onClick={() => {
                            if (cartId) {
                                dispatch(addMovieToCart({ cartId, movieId: movie.id }));
                            }
                            return loaded ? setLoaded(false) : !interval.active && interval.start();
                        }}
                        color={theme.primaryColor}
                    >
                        <div className={classes.label}>
                            {progress !== 0 ? "Add to Cart" : loaded ? "Add to Cart" : "Add to Cart"}
                        </div>
                        {progress !== 0 && (
                            <Progress
                                value={progress}
                                className={classes.progress}
                                color={theme.fn.rgba(theme.colors[theme.primaryColor][2], 0.35)}
                                radius="sm"
                            />
                        )}
                    </Button>
                </Group>
            </Card.Section>
        </Card>
    );
}
