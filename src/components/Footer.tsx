import { Center, Footer as MantineFooter } from "@mantine/core";
import React from "react";
import { Pagination } from "./Pagination";

interface PaginationProps {
    currentPage: number;
    totalPages: number;
}

export function Footer({ currentPage, totalPages }: PaginationProps) {
    return (
        <div>
            <MantineFooter height={60} p="md">
                <Center>
                    <Pagination currentPage={currentPage} totalPages={totalPages} />
                </Center>
            </MantineFooter>
        </div>
    );
}
