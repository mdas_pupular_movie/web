import { Button, Group, Modal, Title } from "@mantine/core";

export function ThanksModal(props: { opened: boolean; onSubmit: () => void; onClick: () => void }) {
    return (
        <Modal opened={props.opened} onSubmit={props.onSubmit} onClose={props.onSubmit}>
            <Title order={3}>Thanks!</Title>
            <Group position="right" style={{ marginTop: 15 }}>
                <Button onClick={props.onClick}>Back to Homepage</Button>
            </Group>
        </Modal>
    );
}
