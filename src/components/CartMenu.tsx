import { useState } from "react";
import { ActionIcon, Group, Popover, useMantineTheme } from "@mantine/core";
import { ShoppingCart } from "tabler-icons-react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { createCart, deleteCart, getCart, removeMovieFromCart, selectCart } from "../features/cart/cart_slice";
import { CheckoutModal } from "./CheckoutModal";
import { ThanksModal } from "./ThanksModal";
import { CartContent } from "./CartContent";

export function CartMenu() {
    const [opened, setOpened] = useState(false);
    const [openedModal1, setOpenedModal1] = useState(false);
    const [openedModal2, setOpenedModal2] = useState(false);
    const theme = useMantineTheme();
    const cartApp = useAppSelector(selectCart);
    const dispatch = useAppDispatch();
    return (
        <Group>
            <CheckoutModal
                opened={openedModal1}
                onSubmit={() => {}}
                onClose={() => {
                    setOpenedModal1(false);
                }}
                onClick={() => {
                    if (cartApp.cart) {
                        dispatch(createCart());
                    }
                    setOpenedModal1(false);
                    setOpenedModal2(true);
                }}
            />
            <ThanksModal
                opened={openedModal2}
                onSubmit={() => {
                    if (cartApp.cart) {
                        dispatch(deleteCart({ cartId: cartApp.cart.cartId }));
                        dispatch(createCart());
                    }

                    setOpenedModal2(false);
                }}
                onClick={() => {
                    if (cartApp.cart) {
                        dispatch(deleteCart({ cartId: cartApp.cart.cartId }));
                        dispatch(createCart());
                    }
                    setOpenedModal2(false);
                }}
            />
            <Popover
                opened={opened}
                onClose={() => setOpened(false)}
                position="bottom"
                placement="end"
                withCloseButton
                title=""
                transition="pop-top-right"
                target={
                    <ActionIcon
                        variant={theme.colorScheme === "dark" ? "hover" : "light"}
                        onClick={() => setOpened((o) => !o)}
                    >
                        <ShoppingCart size={32} />
                    </ActionIcon>
                }
            >
                {cartApp.cart && (
                    <CartContent
                        cart={cartApp.cart}
                        onCancel={() => setOpened(false)}
                        onSubmit={() => {
                            setOpenedModal1(true);

                            setOpened(false);
                        }}
                        onRemoveMovie={(id, quantity) => {
                            try {
                                if (cartApp.cart) {
                                    for (let i = 0; i < quantity; i++) {
                                        dispatch(removeMovieFromCart({ cartId: cartApp.cart.cartId, movieId: id }));
                                    }
                                }
                            } catch (e) {
                                console.log(e);
                            } finally {
                                console.log("update cart");
                                setTimeout(() => {
                                    if (cartApp.cart) {
                                        dispatch(getCart({ cartId: cartApp.cart.cartId }));
                                    }
                                }, 500);
                            }
                        }}
                    />
                )}
            </Popover>
        </Group>
    );
}
