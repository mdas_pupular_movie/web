import React, { useState } from "react";
import { createStyles, Select } from "@mantine/core";
import { useAppDispatch } from "../app/hooks";
import { getMovies } from "../features/movie/movie_slice";

const useStyles = createStyles((theme) => ({
    root: {
        position: "relative"
    },

    input: {
        height: "auto",
        paddingTop: 18
    },

    label: {
        position: "absolute",
        pointerEvents: "none",
        fontSize: theme.fontSizes.xs,
        paddingLeft: theme.spacing.sm,
        paddingTop: theme.spacing.sm / 2,
        zIndex: 1
    }
}));

const genreNames = [
    "Action",
    "Adventure",
    "Animation",
    "Comedy",
    "Crime",
    "Documentary",
    "Drama",
    "Family",
    "Fantasy",
    "Horror",
    "Mystery",
    "Romance",
    "Science Fiction",
    "Thriller",
    "Western"
];

export function ContainedInputs() {
    const { classes } = useStyles();
    const [, setValue] = useState(genreNames[0]);
    const dispatch = useAppDispatch();
    return (
        <div>
            <Select
                clearable
                searchable
                style={{ marginTop: 20, zIndex: 2 }}
                data={genreNames}
                placeholder="Pick one"
                label="Movie Genres"
                classNames={classes}
                onChange={(value) => {
                    if (value) {
                        setValue(value);
                        dispatch(
                            getMovies({
                                page: 1,
                                searchTearm: value?.trim(),
                                searchBy: "genreName"
                            })
                        );
                    } else {
                        dispatch(getMovies({ page: 1, searchTearm: "", searchBy: "genreName" }));
                    }
                }}
            />
        </div>
    );
}
