export type Cart = {
    cartId: string;
    moviesList: CartMovieItem[];
    totalPrice?: number;
};

export type CartMovieItem = {
    id: string;
    title: string;
    price: number;
    quantity?: number;
};
export type GetCartParams = {
    cartId: string;
};

export type DeleteCartParams = {
    cartId: string;
};
export type AddMovieToCartParams = {
    cartId: string;
    movieId: string;
};

export type RemoveMovieFromCartParams = {
    cartId: string;
    movieId: string;
};

export type CartState = {
    cart: Cart | null;
    status: "idle" | "loading" | "failed";
};
