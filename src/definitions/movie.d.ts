export type Movie = {
    id: string;
    cover: string;
    title: string;
    genreName: string;
    price: number;
    year: string;
    rating: number;
    description: string;
};

export type GetMoviesParams = {
    page: number;
    searchTearm?: string;
    searchBy?: "title" | "genreName";
};

export type GetMoviesPageParams = {
    page: number;
};

export type GetMovieByIdParams = {
    id: string;
};

export type MoviesResult = {
    page: number;
    total_pages: number;
    total_results: number;
    results: Movie[];
};
export type MovieState = MoviesResult & {
    status?: Status;
    current_movie?: Movie;
    searchTearm?: string;
    searchBy?: "title" | "genreName";
};
