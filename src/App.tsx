import React from "react";
import { AppShell, useMantineTheme } from "@mantine/core";
import { HeaderMiddle } from "./components/Header";
import { CustomNavbar } from "./components/Nabvar";
import { MovieList } from "./pages/movie_list/MovieList";
import { useAppSelector } from "./app/hooks";
import { selectMovies } from "./features/movie/movie_slice";
import { Footer } from "./components/Footer";

function App() {
    const theme = useMantineTheme();
    const app_movies = useAppSelector(selectMovies);

    return (
        <AppShell
            padding="md"
            styles={{
                main: {
                    background: theme.colorScheme === "dark" ? theme.colors.dark[8] : theme.colors.gray[0]
                }
            }}
            navbarOffsetBreakpoint="sm"
            asideOffsetBreakpoint="sm"
            fixed
            header={<HeaderMiddle />}
            navbar={<CustomNavbar />}
            footer={<Footer currentPage={app_movies.page} totalPages={app_movies.total_pages} />}
        >
            <MovieList />
        </AppShell>
    );
}

export default App;
