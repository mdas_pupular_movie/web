import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import * as MovieAPI from "./movie_api";
import { GetMovieByIdParams, GetMoviesPageParams, GetMoviesParams, Movie, MovieState } from "../../definitions/movie";

const initialState: MovieState = {
    page: 1,
    total_pages: 0,
    total_results: 0,
    results: [],
    status: "idle",
    current_movie: undefined,
    searchTearm: "",
    searchBy: "title"
};

export const getMoviesPage = createAsyncThunk(
    "movie/getMoviesPage",
    async (params: GetMoviesPageParams, { getState }) => {
        // @ts-ignore
        const state = getState().movies;
        const response = await MovieAPI.getMovies({
            page: params.page,
            searchBy: state.searchBy,
            searchTearm: state.searchTearm
        });
        return { movieResult: response.data, searchTearm: state.searchTearm, searchBy: state.searchBy };
    }
);

export const getMovies = createAsyncThunk("movie/getMovies", async (params?: GetMoviesParams) => {
    const response = await MovieAPI.getMovies(params);
    return { movieResult: response.data, searchTearm: params?.searchTearm, searchBy: params?.searchBy };
});

export const getMovieById = createAsyncThunk("movie/getMovieById", async (params: GetMovieByIdParams) => {
    const response = await MovieAPI.getMovieById(params);
    return response.data;
});

export const movieSlice = createSlice({
    name: "movie",
    initialState,
    reducers: {
        setCurrentMovie: (state, action: PayloadAction<Movie>) => {
            state.current_movie = action.payload;
        },
        resetCurrentMovie: (state) => {
            state.current_movie = undefined;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getMovies.pending, (state) => {
                state.status = "loading";
            })
            .addCase(getMovies.fulfilled, (state, action) => {
                const { movieResult, searchTearm, searchBy } = action.payload;
                const { results, page, total_pages, total_results } = movieResult;
                state.results = results;
                state.searchTearm = searchTearm;
                state.searchBy = searchBy;
                state.page = page;
                state.total_pages = total_pages;
                state.total_results = total_results;
                state.status = "idle";
            })
            .addCase(getMovies.rejected, (state) => {
                state.status = "failed";
            })
            .addCase(getMoviesPage.pending, (state) => {
                state.status = "loading";
            })
            .addCase(getMoviesPage.fulfilled, (state, action) => {
                const { movieResult, searchTearm, searchBy } = action.payload;
                const { results, page, total_pages, total_results } = movieResult;
                state.results = results;
                state.searchTearm = searchTearm;
                state.searchBy = searchBy;
                state.page = page;
                state.total_pages = total_pages;
                state.total_results = total_results;
                state.status = "idle";
            })
            .addCase(getMoviesPage.rejected, (state) => {
                state.status = "failed";
            })
            .addCase(getMovieById.pending, (state) => {
                state.status = "loading";
            })
            .addCase(getMovieById.fulfilled, (state, action) => {
                state.current_movie = action.payload;
                state.status = "idle";
            })
            .addCase(getMovieById.rejected, (state) => {
                state.status = "failed";
            });
    }
});
export const { setCurrentMovie, resetCurrentMovie } = movieSlice.actions;
export const selectMovies = (state: RootState) => state.movies;

export default movieSlice.reducer;
