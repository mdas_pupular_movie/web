import {
    AddMovieToCartParams,
    Cart,
    DeleteCartParams,
    GetCartParams,
    RemoveMovieFromCartParams
} from "../../definitions/cart";
import axios from "axios";
import { BASE_URL } from "../../definitions/const";

const instance = axios.create({
    baseURL: BASE_URL
});

export async function createCart(): Promise<{ data: Cart }> {
    const result = await instance.post("/cart");
    const cart: Cart = { cartId: result?.data?.cartId, moviesList: [], totalPrice: 0 };
    return { data: cart };
}

export async function getCart(params: GetCartParams): Promise<{ data: Cart }> {
    const result = await instance.get(`/cart/${params.cartId}`);
    return { data: result.data };
}

export async function addMovieToCart(params: AddMovieToCartParams): Promise<void> {
    await instance.post(`/cart/addMovie`, params);
}

export async function removeMovieFromCart(params: RemoveMovieFromCartParams): Promise<void> {
    await instance.delete(`/cart/removeMovie`, { data: params });
}

export async function deleteCart(params: DeleteCartParams): Promise<void> {
    await instance.delete(`/cart/${params.cartId}`);
}
